#include "myview.h"
#include <QMouseEvent>
#include <QGraphicsRectItem>
#include <QDebug>
myview::myview()
{
    scene = new QGraphicsScene;
    setScene(scene);
    setMouseTracking(true);

    setFixedSize(400,500);

    setAlignment(Qt::AlignLeft | Qt::AlignTop);

    QPen outlinePen(Qt::black);
    outlinePen.setWidth(2);
    scene->addLine(QLineF(0,0,0,200),outlinePen);
    scene->addLine(QLineF(0,0,100,0),outlinePen);

    selection = new QGraphicsPolygonItem();
    selection->setVisible(false);
    scene->addItem(selection);
}

void myview::mouseMoveEvent(QMouseEvent *e) {
    dragEnd = e->pos(); // this will give you the pos relative to the widget's top-left cornor
    int dx = dragEnd.x()-dragBegin.x();
    int dy = dragEnd.y()-dragBegin.y();

    // TODO: this doesn't work if the view is scrolled and moved.

    int aw = abs(dx);
    int ah = abs(dy);
    if (dx>0 && dy >0) {
        selection->setPolygon(mapToScene(QRect(dragBegin.x(), dragBegin.y(), aw, ah)));
    } else if (dx < 0 && dy > 0){
        selection->setPolygon(mapToScene(QRect(dragEnd.x(),dragBegin.y(),aw,ah)));
    } else if (dx > 0 && dy < 0) {
        selection->setPolygon(mapToScene(QRect(dragBegin.x(),dragEnd.y(),aw,ah)));
    } else {
        selection->setPolygon(mapToScene(QRect(dragEnd.x(),dragEnd.y(),aw,ah)));
    }
}

void myview::mousePressEvent(QMouseEvent *e) {
    auto x = e->x();
    auto y = e->y();
    dragBegin = e->pos();
    selection->setVisible(true);

    auto apoint = new QGraphicsRectItem(QRectF(mapToScene(x,y),QSizeF(10,10)));

    scene->addItem(apoint);
}

void myview::mouseReleaseEvent(QMouseEvent *e) {
    selection->setVisible(false);

}

